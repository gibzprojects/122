﻿Clear-Host

$ColouredServicesAsHtml = Get-Service | ConvertTo-Html -Property Name, Status | 
    ForEach-Object { 
        if ($_ -like "*<td>Running</td>*") { 
            $_ -replace "<tr>", "<tr style='background-color: green;'>" 
        } 
        else { 
            $_ -replace "<tr>", "<tr style='background-color: red;'>"  
        }
    }

$ColouredServicesAsHtml | Out-File -FilePath C:\Users\luis.leuppi\source\powershell_scripts\122\Intro\test.html