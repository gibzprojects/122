﻿Set-Location .\.xls

Get-ChildItem | ForEach-Object { $file = Get-Item $_.FullName; $file.IsReadOnly = 0 }

Set-Location ..