﻿Clear-Host

Get-ChildItem | Select-Object Extension -Unique | ForEach-Object { New-Item -ItemType Directory -Name $_.Extension }
