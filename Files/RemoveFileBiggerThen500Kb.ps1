﻿Clear-Host

Get-ChildItem | Where-Object { ($_.Length / 1KB)  -gt 500 } | ForEach-Object { Remove-Item $_.FullName }